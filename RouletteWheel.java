import java.util.Random;

public class RouletteWheel {

    private Random rand;
    private int lastSpin;

    public RouletteWheel() {
        rand = new Random();
        lastSpin = 0;
    }

    public void spin() {
        lastSpin = rand.nextInt(37);
    }

    public int getValue() {
        return lastSpin;
    }

}