import java.util.Scanner;
public class Roulette {

    public static void main(String args[]) {
        RouletteWheel wheel = new RouletteWheel();
        Scanner scan = new Scanner(System.in);
        
        //Messages and storing
        System.out.println("How much would you like to bet?");
        int bet = scan.nextInt();
        System.out.println("Would you like to bet? (Y/N)");
        char answer = scan.next().charAt(0);
        System.out.println("What number would you like to bet? (0 to 37)");
        int slotBet = scan.nextInt();

        //Spinning
        wheel.spin();
        int rolled = wheel.getValue();
        System.out.println(rolled);

        if (rolled == slotBet) {
            int won = bet * (slotBet--);
            System.out.println("You win" + won + "!");
        }
        else {
            System.out.println("haha you lost money");
        }

    }
}